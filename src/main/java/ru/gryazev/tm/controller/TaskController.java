package ru.gryazev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.enumerated.Status;
import ru.gryazev.tm.repository.IProjectRepository;
import ru.gryazev.tm.repository.ITaskRepository;
import ru.gryazev.tm.util.DateUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class TaskController {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    @Autowired
    public TaskController(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @GetMapping("tasks")
    public String getProjects(Map<String, Object> model) {
        @NotNull final List<TaskEntity> tasks = new ArrayList<>();
        taskRepository.findAll().forEach(tasks::add);
        model.put("tasks", tasks);
        return "task/tasks";
    }

    @PostMapping({"add-task", "edit-task"})
    public String addTask(
            @RequestParam("linkedProjectId") String projectId,
            @RequestParam(value = "taskId", required = false) String taskId,
            @RequestParam("name") String name,
            @RequestParam("details") String details,
            @RequestParam("dateStart") String dateStart,
            @RequestParam("dateFinish") String dateFinish,
            @RequestParam("status") String status,
            Map<String, Object> model
    ) {
        @NotNull final TaskEntity task = new TaskEntity();
        if (taskId != null) {
            task.setId(taskId);
        }
        task.setProject(projectRepository.findById(projectId).orElse(null));
        task.setName(name);
        task.setDetails(details);
        task.setDateStart(DateUtils.formatStringToDate(dateStart));
        task.setDateFinish(DateUtils.formatStringToDate(dateFinish));
        task.setStatus(Status.valueOf(status));
        taskRepository.save(task);
        @NotNull final List<TaskEntity> tasks = new ArrayList<>();
        taskRepository.findAll().forEach(tasks::add);
        model.put("tasks", tasks);
        return "redirect:/tasks";
    }

    @GetMapping("add-task")
    public String addTask(
            Map<String, Object> model
    ) {
        model.put("projects", projectRepository.findAll());
        model.put("statuses", Status.values());
        return "task/add-task";
    }

    @GetMapping("edit-task")
    public String editProject(
            @RequestParam("taskId") String taskId,
            Map<String, Object> model
    ) {
        @Nullable final TaskEntity task = taskRepository.findById(taskId).orElse(null);
        if (task == null) return "redirect:tasks";
        model.put("projects", projectRepository.findAll());
        model.putAll(getModelByTask(task));
        return "task/edit-task";
    }

    @PostMapping("task-remove")
    public String removeTask(
            @RequestParam("taskId") String taskId,
            Map<String, Object> model
    ) {
        taskRepository.deleteById(taskId);
        @NotNull final List<TaskEntity> tasks = new ArrayList<>();
        taskRepository.findAll().forEach(tasks::add);
        model.put("tasks", tasks);
        return "redirect:/tasks";
    }

    @GetMapping("view-task")
    public String viewTask(
            @RequestParam("taskId") String taskId,
            Map<String, Object> model
    ) {
        @Nullable final TaskEntity task = taskRepository.findById(taskId).orElse(null);
        if (task == null) return "redirect:tasks";
        model.putAll(getModelByTask(task));
        return "task/view-task";
    }

    public Map<String, Object> getModelByTask(final TaskEntity task) {
        Map<String, Object> model = new HashMap<>();
        @Nullable final String dateStart = DateUtils.formatDateToString(task.getDateStart());
        @Nullable final String dateFinish = DateUtils.formatDateToString(task.getDateFinish());
        model.put("task", task);
        model.put("dateStart", dateStart);
        model.put("dateFinish", dateFinish);
        model.put("statuses", Status.values());
        return model;
    }

}
