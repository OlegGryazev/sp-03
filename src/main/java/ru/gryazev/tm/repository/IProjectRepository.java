package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.gryazev.tm.entity.ProjectEntity;

import java.util.Optional;

@Repository
public interface IProjectRepository extends CrudRepository<ProjectEntity, String> {
    
    @NotNull
    public Optional<ProjectEntity> findById(@NotNull String id);

    public void deleteById(@NotNull String id);

}
