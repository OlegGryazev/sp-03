<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Project Manager</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="projects">Projects</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="tasks">Tasks</a>
            </li>
        </ul>
    </div>
</nav>