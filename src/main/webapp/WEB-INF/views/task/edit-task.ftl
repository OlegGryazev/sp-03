<#import "../parts/common.ftl" as c>
<@c.page>
    <div class="col-5">
        <form method="post" action="edit-task">
            <input type="hidden" name="taskId" value="${task.getId()}" />
            <div class="form-group">
                <label for="linkedProjectId">Linked project</label>
                <select class="form-control" name="linkedProjectId" id="linkedProjectId" >
                    <#list projects as project>
                        <#if project.getId() == task.getProject().getId()>
                            <option value="${project.getId()}" selected>${project.getName()}</option>
                        <#else>
                            <option value="${project.getId()}">${project.getName()}</option>
                        </#if>
                    </#list>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Task name</label>
                <input class="form-control mb-2" type="text" id="name" name="name" value="${task.getName()}" placeholder="Task name" />
            </div>
            <div class="form-group">
                <label for="details">Task details</label>
                <input class="form-control mb-2" type="text" id="details" name="details" value="${task.getDetails()}" placeholder="Task details" />
            </div>
            <div class="form-row">
                <div class="form-group col">
                    <label for="dateStart">Date of task start</label>
                    <#if dateStart??>
                        <input class="form-control mb-2" type="date" id="dateStart" name="dateStart" value="${dateStart}" />
                    <#else>
                        <input class="form-control mb-2" type="date" id="dateStart" name="dateStart" />
                    </#if>
                </div>
                <div class="form-group col">
                    <label for="dateFinish">Date of task finish</label>
                    <#if dateFinish??>
                        <input class="form-control mb-2" type="date" id="dateFinish" name="dateFinish" value="${dateFinish}" />
                    <#else>
                        <input class="form-control mb-2" type="date" id="dateFinish" name="dateFinish" />
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label for="status">Task status</label>
                <select class="form-control" name="status" id="status" >
                    <#list statuses as status>
                        <#if status == task.getStatus()>
                            <option value="${status}" selected>${status.displayName()}</option>
                        <#else>
                            <option value="${status}">${status.displayName()}</option>
                        </#if>
                    </#list>
                </select>
            </div>
            <div class="form-row mt-4">
                <button class="btn btn-primary mr-3" type="submit">Edit task</button>
                <a href="tasks" class="btn btn-secondary">Cancel</a>
            </div>
        </form>
    </div>
</@c.page>